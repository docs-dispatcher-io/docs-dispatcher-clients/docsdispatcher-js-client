import { asList } from '../../util/helpers'
import { Target, Targetable, TargetablePayload } from '../interfaces/generics'

export type TargetablePreset = { targets?: Target[] }

export default class TargetableMixin implements Targetable {

  protected targets: Target[] = []

  public addTarget(target: Target): this {
    this.targets = asList<Target>(this.targets).concat([target])
    return this
  }

  public addTargets(targets: Target[]): this {
    this.targets = asList<Target>(this.targets).concat(targets)
    return this
  }

  public asTargetablePayload(): TargetablePayload {
    return {
      targets: asList<Target>(this.targets).length ? this.targets : void 0
    }
  }

  protected setTargetablePreset(preset: TargetablePreset): void {
    const { targets } = preset
    Object.assign(this, { targets })
  }
}