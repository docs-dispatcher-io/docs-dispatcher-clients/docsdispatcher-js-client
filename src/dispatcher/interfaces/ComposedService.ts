import { ValidationError } from '../../validation/interfaces'

export default interface ComposedService {
  getPathSegment(): string;
  validate(errors: ValidationError[]): boolean;
}