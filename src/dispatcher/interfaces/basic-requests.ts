// @eslint-disable max-class-per-file
import { applyMixins } from '../../util/apply-mixins'
import TemplatableMixin, { TemplatePreset } from '../mixins/TemplatableMixin'
import { PayloadGenerator, TemplatePayload } from './generics'

export type BasicRequestPayload = TemplatePayload

export class BasicRequest implements PayloadGenerator<BasicRequestPayload> {
  public constructor(preset: TemplatePreset) {
    if (preset) {
      this.setTemplatePreset(preset)
    }
  }

  public asPayload(): BasicRequestPayload {
    return this.asTemplatePayload()
  }
}

applyMixins(BasicRequest, [TemplatableMixin])
export interface BasicRequest extends TemplatableMixin { }