export type AbstractData = Record<string, unknown>
export type MaybeData = AbstractData | undefined

export interface HasData {
  setData(data: AbstractData): unknown;
}

export type Payload = Record<string, unknown>

export type TemplatePayload = Payload & {
  templateName?: string;
  resultFileName?: string;
  data?: AbstractData;
}

export interface Templatable extends HasData {
  setTemplateName(templateName: string): this;
  setResultFileName(resultFileName: string): this;
}
export enum TargetTypes{
  ZohoCrm = 'zohoCRMUpload',
  Gema = 'gemaUpload'
}

export type Target = {
  id: string;
  type: string;
  target: TargetTypes;
};

export type TargetablePayload = Payload & {
  targets?: Target[];
}

export interface Targetable {
  addTarget (target: Target): this;

  addTargets(targets: Target[]): this;
}

export interface PayloadGenerator<P> {
  asPayload(): P;
}

export type WithSettingsPayload = {
  settings?: MaybeData;
}

export interface WithSettings {
  setSettings (settings: MaybeData): this;
  addSetting (key: string, value: unknown): this;
}