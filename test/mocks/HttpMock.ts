import HttpAdapter from '../../src/http/interfaces/HttpAdapter'
import RequestDescriptor from '../../src/http/interfaces/RequestDescriptor'
import HttpResponse from '../../src/http/interfaces/HttpResponse'

export default class HttpMock implements HttpAdapter {
  private onRequest: (req: RequestDescriptor) => Promise<HttpResponse>

  constructor(onRequest: (req: RequestDescriptor) => Promise<HttpResponse>) {
    this.onRequest = onRequest
  }

  send(req: RequestDescriptor): Promise<HttpResponse> {
    return this.onRequest(req)
  }
}
