/* eslint-disable no-process-env */
import axios from 'axios'
import { config as loadEnv } from 'dotenv'
import BasicAuth from '../../src/auth/BasicAuth'
import { Payload } from '../../src/dispatcher/interfaces/generics'
import AxiosAdapter from '../../src/http/adapters/AxiosAdapter'
import DocsDispatcher from '../../src/main'

loadEnv()

describe('Check live env', () => {
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_USERNAME).toBe('string')
    expect(process.env.LIVE_USERNAME).not.toBeFalsy()
  })
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_PASSWORD).toBe('string')
    expect(process.env.LIVE_PASSWORD).not.toBeFalsy()
  })
  it('sould have environment setup', () => {
    expect(typeof process.env.LIVE_ENDPOINT).toBe('string')
    expect(process.env.LIVE_ENDPOINT).not.toBeFalsy()
  })
})

export default function makeLiveDD<P extends Payload>(): DocsDispatcher<P> {

  const liveUsername = process.env.LIVE_USERNAME
  const livePassword = process.env.LIVE_PASSWORD
  const liveEndpoint = process.env.LIVE_ENDPOINT

  const auth = new BasicAuth(String(liveUsername), String(livePassword))
  const http = new AxiosAdapter(axios)

  return new DocsDispatcher<P>(auth, http, {
    endpoint: liveEndpoint
  })
}