/* eslint-disable no-process-env */
const { TEST_LIVE } = process.env
const live = String(TEST_LIVE) === '1'
module.exports = {
  transform: {
    '.(ts|tsx)': 'ts-jest',
  },
  testEnvironment: 'node',
  testRegex:
    '(/__tests__/.*|\\.(test|spec' + (live ? '|live' : '') + '))\\.(ts|js)$',
  moduleFileExtensions: ['ts', 'js'],
  coveragePathIgnorePatterns: ['/node_modules/', '/test/'],
  coverageThreshold: {
    global: {
      branches: 50,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  collectCoverageFrom: ['src/**/*.{js,ts}'],
}
