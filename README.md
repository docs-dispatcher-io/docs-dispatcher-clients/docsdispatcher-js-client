[![pipeline status](https://gitlab.com/docs-dispatcher-io/docs-dispatcher-clients/docsdispatcher-js-client/badges/main/pipeline.svg)](https://gitlab.com/docs-dispatcher-io/docs-dispatcher-clients/docsdispatcher-js-client/-/commits/main) [![coverage report](https://gitlab.com/docs-dispatcher-io/docs-dispatcher-clients/docsdispatcher-js-client/badges/main/coverage.svg)](https://docs-dispatcher-io.gitlab.io/docs-dispatcher-clients/docsdispatcher-js-client/coverage/)

# DocsDispatcher JS Client

## SDK Usage

```
yarn add docs-dispatcher-sdk
```

or

```
npm install docs-dispatcher-sdk
```

See [Documentation](./docs/) or [online on Gilab.com](https://docs-dispatcher-io.gitlab.io/docs-dispatcher-clients/docsdispatcher-js-client/doc/)

See [API documentation](https://api.docs-dispatcher.io/docs), also available as [Swagger format](https://api.docs-dispatcher.io/docs/swagger/).

## Development

### Onboarding

create new `.env` file from `.env.dist` to execute tests

## Publishing new version

- `yarn version --new-version x.y.z`
- rebase main on this tag
- trigger pipeline on main
